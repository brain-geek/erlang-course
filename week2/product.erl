- module(product).
- export([evaluate/1, evaluate_tail_recursion/1]).

% Using the template from the last session, define an Erlang function to give the product of a list of numbers.
% The product of an empty list is usually taken to be 1: why?

evaluate([]) ->
  1;
evaluate([HEAD | TAIL]) ->
  HEAD * evaluate(TAIL).

evaluate_tail_recursion(LIST) ->
  evaluate_tail_recursion(LIST, 1).

evaluate_tail_recursion([], ACC) ->
  ACC;
evaluate_tail_recursion([HEAD | TAIL], ACC) ->
  evaluate_tail_recursion(TAIL, ACC * HEAD).
