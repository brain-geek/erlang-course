-module(palindrome).
-export([check/1]).

check([], []) ->
  true;

check(_A, []) ->
  false;

check([], _B) ->
  false;

check([A | As], [B | Bs]) ->
  case equal(A, B) of
    true -> check(As, Bs);
    false -> false
  end.

check(A) ->
  ClearedA = clearString(A),
  ReversedA = lists:reverse(ClearedA),
  check(ClearedA, ReversedA).

equal(A, B) ->
  string:equal([A], [B], true).

clearString([]) ->
  [];

clearString([32 | Xs]) -> % space
  clearString(Xs);

clearString([39 | Xs]) -> % ' symbol
  clearString(Xs);

clearString([X | Xs]) ->
  [X | clearString(Xs)].
