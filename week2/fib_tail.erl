-module(fib_tail).
-export([fib/1]).

fib(0) ->
  0;
fib(1) ->
  1;
fib(2) ->
  1;
fib(N) ->
  fib(N - 2, 1, 1).

fib(0, VAL, _) ->
  VAL;
fib(N, PREV, PREVPREV) ->
  fib(N-1, PREV + PREVPREV, PREV).
