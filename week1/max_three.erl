-module(max_three).
-export([max_three/3]).

max_three(A, B, C) ->
  max_two(max_two(A,B), C).

max_two(A, B) ->
  if
    A > B ->
      A;
    true ->
      B
  end.
