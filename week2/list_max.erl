- module(list_max).
- export([calc/1, calc_tail_recursion/1]).

calc([A]) ->
  A;

calc([A | TAIL]) ->
  max(A, calc(TAIL)).

calc_tail_recursion(LIST = [FIRST | _TAIL]) ->
  calc_tail_recursion(LIST, FIRST).

calc_tail_recursion([], ACC) ->
  ACC;

calc_tail_recursion([CURR | TAIL], ACC) ->
  calc_tail_recursion(TAIL, max(ACC, CURR)).
