-module(second).
-export([hypotenuse/2, perimeter/3, area/3]).


% Using the function square from first.erl, define a function that
% gives the size of the hypotenuse of a right-angled triangle given the lengths of the two other sides.

hypotenuse(A,B) ->
  SQ = A*A + B*B,
  math:sqrt(SQ).

% Define functions that give the perimeter and area of a right-angled triangle, given the lengths of the two short sides.

perimeter(A,B,C) ->
  A + B + C.

area(A,B,C) ->
  first:area(A,B,C).
