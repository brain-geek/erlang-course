- module(constructing_lists).
- export([double/1, evens/1]).

% Transforming list elements
% Define an Erlang function double/1 to double the elements of a list of numbers.

double([]) ->
  [];

double([H | Tail]) ->
  [H * 2 | double(Tail)].

% Filtering lists
% Define a function evens/1 that extracts the even numbers from a list of integers.

evens([]) ->
  [];

evens([H | Tail]) when (H rem 2 == 0) ->
  [H | evens(Tail)];

evens([_H | Tail]) ->
  evens(Tail).
