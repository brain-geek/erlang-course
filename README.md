# 6 weeks of Erlang - Log


### May 5, 2020
##### Course starts for me


**Today's Progress**:
 - Started the course!
 - Watched first three videos

**Thoughts:** :

Getting to speed. I need some teammates to catch up to. :)

**Stack:** :

**Link to work:**



### May 6, 2020
##### Course starts for me


**Today's Progress**:
 - Spent three hours trying to make asdf version of Erlang compile. Didn't work out in the end. :(
 - Watched next few videos
 - Wrote first Erlang code in this course
 - Third of first week is done!

**Thoughts:** :

Lecturer using wording like `has a more free-wheeling approach then Java` shows that that's still introductions.

**Stack:** :

**Link to work:**


### May 8, 2020
##### Course starts for me


**Today's Progress**:
 - Two more videos done.
 - First Erlang code finally written by myself!
 - First week is halfway through. Awesome, I see progress.

**Thoughts:** :

Erlang code is weird. But concept of ending lines with commas and finishing up function with dot is even funny. Definitely oldschool.

**Stack:** :

**Link to work:**

see week1/second.erl



### May 13, 2020
##### After overtiming at work for last few days, and not having time for anything, I'm back


**Today's Progress**:
 - 3 or 4 videos done
 - I plan to finish week 1 tomorrow.

**Thoughts:** :

none yet

**Stack:** :

**Link to work:**


### May 14, 2020

**Today's Progress**:
 - Finished week 1.

**Thoughts:** :

Seems like I need to get to Erlang coding styles, at the moment it's quite confusing.

**Stack:** :

**Link to work:**


### May 18, 2020

**Today's Progress**:
 - Finished first third of week 2.

**Thoughts:** :

Okay, amount of coding in week 2 is definitely higher then in week 1. Awesome. :)

**Stack:** :

**Link to work:**


### May 20, 2020

**Today's Progress**:
 - Finished first coding assignment (2.8).
 - Finished third of the week 2.

**Thoughts:** :

I don't have a clue why, but for some reason Erlang code - as well as Elixir code - ends up a lot less buggy then on other languages for me. Generally, I write it down and it works as intended in ~50% of cases. With other languages for me it's usually ~10%. Why?

Hope to finish week 2 in the next few days.

**Stack:** :

**Link to work:**


### May 24, 2020

**Today's Progress**:
 - Functions over list.
 - Halfway through week 2.

**Thoughts:** :

Where to get more time? Anyone selling time machine?

**Stack:** :

**Link to work:**



### May 27, 2020

**Today's Progress**:
 - Few more lessons.
 - Added my version of palindrome from before watching the lesson.

**Thoughts:** :

Palindrome lesson was too long, though interesting. I didn't even think about math operations on ascii codes before lecturer suggested that.
