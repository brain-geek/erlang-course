-module(how_many_equal).
-export([how_many_equal/3]).


how_many_equal(A, A, A) ->
  3;
how_many_equal(_B, A, A) ->
  2;
how_many_equal(A, _B, A) ->
  2;
how_many_equal(A, A, _B) ->
  2;
how_many_equal(_A, _B, _C) ->
  0.
