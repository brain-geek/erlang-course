-module(nub).
-export([nub/1, removeAll/2]).

nub([]) ->
  [];

nub([X | Xs]) ->
  [X | nub(removeAll(X, Xs))].

removeAll(_X, []) ->
  [];

removeAll(X, [X | Xs]) ->
  removeAll(X, Xs);

removeAll(X, [Y | Xs]) ->
  [Y | removeAll(X, Xs)].
