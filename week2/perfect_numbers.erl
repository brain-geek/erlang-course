-module(perfect_numbers).
-export([is_perfect/1]).

is_perfect(VAL) ->
  sum_of_divisors(VAL, 0, VAL - 1) == VAL.

sum_of_divisors(_VAL, ACCUMULATOR, 0) ->
  ACCUMULATOR;

sum_of_divisors(_VAL, ACCUMULATOR, 1) ->
  ACCUMULATOR + 1;

sum_of_divisors(VAL, ACCUMULATOR, CURRENT_STEP) when VAL rem CURRENT_STEP == 0 ->
  sum_of_divisors(VAL, ACCUMULATOR + CURRENT_STEP, CURRENT_STEP - 1);

sum_of_divisors(VAL, ACCUMULATOR, CURRENT_STEP) when VAL > 0 ->
  sum_of_divisors(VAL, ACCUMULATOR, CURRENT_STEP - 1).
