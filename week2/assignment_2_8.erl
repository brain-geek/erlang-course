-module(assignment_2_8).
-include_lib("eunit/include/eunit.hrl").
-export([perimeter/1, area/1, enclose/1, bits/1]).

% To run all tests, execute assignment_2_8:test() after loading this file.

% This file is split into few parts, where each part is one task.
% Due to submission system limitation, I merged all functions into one module.

%
% As this hasn't been specified in assignment, I've decided to support three shapes:
%
% triangle:
% { triangle, {X, Y}, {X, Y}, {X, Y} }
% where three coordinates describe positions of all points in triangle
%
% circle:
% { circle, {X, Y}, VAL_radius}
% where X and Y are center position
%
% rectangle:
% { rectangle, {X,Y}, {X,Y}}
% where coordinates describe two opposite edges of rectangle
%

% PERIMETER

% See perimeter_test/0 for examples how to use perimeter function and test cases

perimeter({triangle, {X1, Y1}, {X2, Y2}, {X3, Y3}}) ->
hypotenuse({X1, Y1}, {X2, Y2}) + hypotenuse({X2, Y2}, {X3, Y3}) + hypotenuse({X1, Y1}, {X3, Y3});

perimeter({circle, {_X, _Y}, RADIUS}) ->
math:pi() * 2 * RADIUS;

perimeter({rectangle, {X1,Y1}, {X2,Y2}}) ->
(abs(X1 - X2) + abs(Y1 - Y2)) * 2.

perimeter_test() ->
?assertEqual(perimeter({triangle, {0, 3}, {4, 0}, {0, 0}}), 12.0),
?assertEqual(perimeter({triangle, {1, 4}, {5, 1}, {1, 1}}), 12.0),
?assertEqual(perimeter({triangle, {0, 4}, {0, 1}, {0, 2}}), 6.0),

?assertEqual(perimeter({circle, {0, 0}, 2}), math:pi() * 4),

?assertEqual(perimeter({rectangle, {0, 0}, {2, 2} }), 8),
?assertEqual(perimeter({rectangle, {-2, -2}, {2, 2} }), 16),
?assertEqual(perimeter({rectangle, {2, 2}, {1, 1} }), 4).

% AREA

% See area_test/0 for examples how to use enclose function and test cases

% While this is not explicitly stated in task:
% > Choose a suitable representation of triangles, and augment area/1 ... to handle this case too.
% I decided to include the area function in this assignment.

area({triangle, {X1, Y1}, {X2, Y2}, {X3, Y3}}) ->
A = hypotenuse({X1, Y1}, {X2, Y2}),
B = hypotenuse({X2, Y2}, {X3, Y3}),
C = hypotenuse({X1, Y1}, {X3, Y3}),
S = (A+B+C)/2,
math:sqrt(S*(S-A)*(S-B)*(S-C));

area({circle, {_X, _Y}, RADIUS}) ->
math:pi() * RADIUS * RADIUS;

area({rectangle, {X1,Y1}, {X2,Y2}}) ->
(abs(X1 - X2) * abs(Y1 - Y2)).

area_test() ->
?assertEqual(area({triangle, {1, 5}, {4, 5}, {4, 1}}), 6.0),

?assertEqual(area({circle, {5, 5}, 2}), math:pi() * 4),

?assertEqual(area({rectangle, {1, 1}, {3, 4} }), 6).

% See enclose_test/0 for examples how to use enclose function and test cases

% ENCLOSE

enclose({triangle, {X1, Y1}, {X2, Y2}, {X3, Y3}}) ->
{rectangle, {min(X1, X2, X3), min(Y1, Y2, Y3)}, {max(X1, X2, X3), max(Y1, Y2, Y3)} };

enclose({circle, {X, Y}, RADIUS}) ->
{rectangle, {X - RADIUS, Y - RADIUS}, {X + RADIUS, Y + RADIUS}};

enclose({rectangle, {X1,Y1}, {X2,Y2}}) ->
{rectangle, {min(X1, X2), min(Y1, Y2)}, {max(X1, X2), max(Y1, Y2)}}.

enclose_test() ->
?assertEqual(enclose({triangle, {0, 3}, {4, 0}, {0, 0}}), {rectangle, {0, 0}, {4, 3} }),
?assertEqual(enclose({triangle, {1, 4}, {5, 1}, {1, 1}}), {rectangle, {1, 1}, {5, 4} }),

?assertEqual(enclose({circle, {0, 0}, 2}), {rectangle, {-2, -2}, {2, 2} }),

?assertEqual(enclose({rectangle, {2, 0}, {0, 2} }), {rectangle, {0, 0}, {2, 2} }),
?assertEqual(enclose({rectangle, {-2, 2}, {2, -2} }), {rectangle, {-2, -2}, {2, 2} }),
?assertEqual(enclose({rectangle, {2, 2}, {1, 1} }), {rectangle, {1, 1}, {2, 2} }).

% BITS

% See bits_test/0 for examples how to use enclose function and test cases

bits(N) when N > 0 ->
bits(N, 0).

bits(0, INC) ->
INC;

bits(N, INC) ->
bits(N div 2, INC + (N rem 2)).

bits_test() ->
?assertEqual(bits(1), 1),
?assertEqual(bits(2), 1),
?assertEqual(bits(3), 2),

?assertEqual(bits(7), 3),
?assertEqual(bits(8), 1),

?assertEqual(bits(1023), 10),
?assertEqual(bits(1024), 1).

% Helper functions

hypotenuse({X1, Y1}, {X2, Y2}) ->
X_DIFF = X1 - X2,
Y_DIFF = Y1 - Y2,
math:sqrt(X_DIFF * X_DIFF + Y_DIFF * Y_DIFF).

max(A, B, C) ->
max(max(A,B), C).

min(A, B, C) ->
min(min(A,B), C).

